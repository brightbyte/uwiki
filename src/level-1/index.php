<!DOCTYPE html>
<html lang="en">
<head>
	<title>uWiki</title>
	<meta charset="utf-8" />
</head>

<body>
<h1>uWiki</h1>
<p class="content">
	<?php
	$text = file_get_contents( __DIR__ . '/content.txt' );
	print( htmlspecialchars( $text ) );
	?>
</p>
<p><a href="./edit.php" class="button">Edit</a></p>
</body>
</html>
