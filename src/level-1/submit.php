<?php
function make_url( $subpath ) {
	$requestUri = "http://" . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . $_SERVER['REQUEST_URI'];
	$requestUriParts = parse_url( $requestUri );
	$path = preg_replace('#/[^/]*$#', '', $requestUriParts['path'] );
	$baseUri = $requestUriParts['scheme'] . '://' . $requestUriParts['host'] . ':' . $requestUriParts['port'] . $path ;
	return $baseUri . $subpath;
}

if ( isset( $_POST['text'] ) ) {
	file_put_contents( __DIR__ . '/content.txt', $_POST['text'] );
}

$redirect = make_url( '' );
header( "Location: $redirect", true, 303 );
print $redirect;
