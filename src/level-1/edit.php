<!DOCTYPE html>
<html lang="en">
<head>
    <title>uWiki (Edit)</title>
    <meta charset="utf-8" />
</head>

<body>
    <h1>Edit uWiki</h1>
    <form class="edit-form" action="./submit.php" method="post">
        <textarea name="text" rows="20" cols="50"><?php
            $text = file_get_contents( __DIR__ . '/content.txt' );
            print( htmlspecialchars( $text ) );
        ?></textarea>
        <input type="submit" value="Save"/>
    </form>
</body>
</html>
